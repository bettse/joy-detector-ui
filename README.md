
Uses an AIY Vision (raspberry pi 0 + machine learning chip) with a "joy detector" model, records the score to influx, and a frontend on Netlify shows an emoji based on the max of the last 10 minutes.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[![Netlify Status](https://api.netlify.com/api/v1/badges/c45586d5-3023-4b7d-9c09-e6a1a8630835/deploy-status)](https://app.netlify.com/sites/joy-detector-ui/deploys)

## Development

In the project directory, you can run:

### `netlify dev`

Runs the app in the development mode.<br />
Open [http://localhost:9000](http://localhost:9000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.



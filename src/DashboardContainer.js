import React, { useEffect, useState } from 'react';

import { TimeSeries } from "pondjs";

import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

import Emoji from './Emoji'
import Dashboard from './Dashboard'

function DashboardContainer(props) {
  const [series, setSeries] = useState(null);

  useEffect(() =>  {
    async function fetchData() {
      try {
        // TODO: Check if page is visible, skip update if not
        const response = await fetch(`/.netlify/functions/series`);
        if (response.ok) {
          const data = await response.json();
          setSeries(data);
        } else {
          console.log('Response was not OK', await response.text());
        }
      } catch (err) {
        console.error('fetch data err', err);
      }
    }
    setInterval(fetchData, 60*1000);
    fetchData();
  }, []);

  if (series === null) {
    return (
      <>
        <Row>
          <Col>
            <Emoji value={null} />
          </Col>
        </Row>
      </>
    );
  }

  if (series.length === 0) {
    return (
      <>
        <Row>
          <Col>
            <Emoji value={undefined} />
          </Col>
        </Row>
      </>
    );
  }

  // Find the series whose last data point has the more recent timestamp
  series.sort((a, b) => b.points[b.points.length - 1][0] - a.points[a.points.length - 1][0])
  const current = series[0]
  const location = current.tags.host

  const ts = new TimeSeries(current)

  const aligned = ts.align({
    fieldSpec: 'value',
    period: '1s',
    method: 'linear',
    limit: 1,
  })

  return (
    <Dashboard location={location} series={aligned} />
  );
}

export default DashboardContainer;

import React from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Alert from 'react-bootstrap/Alert';

import Emoji from './Emoji';
import JoyChart from './JoyChart';

const locationToEmoji = {
  home: '🏠',
  work: '👨‍💻',
};
function Dashboard(props) {
  const {series, location} = props;
  const p99 = series.percentile(99, 'value', 'midpoint') || 0;
  return (
    <>
      <Row>
        <Col>
          <Emoji value={p99} />
        </Col>
      </Row>
      <Row className="text-center justify-content-center mt-1">
        <Col xs={3}>
          <Alert variant="info">
            <h5>
              <span role="img" aria-label={location} title={location}>
                {locationToEmoji[location]}
              </span>
              &nbsp; Last seen at {location} &nbsp;
              <span role="img" aria-label={location} title={location}>
                {locationToEmoji[location]}
              </span>
            </h5>
          </Alert>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <Card.Header>Chart</Card.Header>
            <Card.Body>
              <JoyChart series={series} />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
}
export default Dashboard;

import React from 'react';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import DashboardContainer from './DashboardContainer';
import './App.css';

function App() {
  return (
    <div className="App">
      <Navbar collapseOnSelect expand="sm" bg="info" variant="info">
        <Navbar.Brand>Joy score</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="https://gitlab.com/bettse/joy-detector-ui/">
              Sourcecode (GitLab)
            </Nav.Link>
          </Nav>
          <Nav>
            <Navbar.Text>
              <img
                src="https://api.netlify.com/api/v1/badges/c45586d5-3023-4b7d-9c09-e6a1a8630835/deploy-status"
                alt="Netlify Status"
              />
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container>
        <DashboardContainer />
        <Row>
          <Col>
            <Card>
              <Card.Header>Details</Card.Header>
              <Card.Body>
                {`This project uses an AIY Vision (Raspberry Pi 0 + Machine learning), running a modified version of the 'Joy Detector demo', which continually reports the joy score to influxdb.  This frontend, hosted on Netlify, requests the scores and synthesizes them down to a single emoji.`}
              </Card.Body>
            </Card>
          </Col>
        </Row>

      </Container>
      <footer className="footer font-small mx-auto pt-5">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
              <small className="text-muted">
                Hosted with <a href="https://www.netlify.com/">Netlify</a>
              </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </div>
  );
}

export default App;

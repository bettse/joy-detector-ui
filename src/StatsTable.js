import React from 'react';

import Table from 'react-bootstrap/Table'

import { mapValueToEmoji } from './Emoji'

function StatsTable(props) {
  const { series } = props;

  const min = series.min() || 0;
  const mean = series.mean() || 0;
  const max = series.max() || 0;
  const median = series.median() || 0;
  const p95 = series.percentile(95, 'value', 'midpoint') || 0;
  const p975 = series.percentile(97.5, 'value', 'midpoint') || 0;
  const p99 = series.percentile(99, 'value', 'midpoint') || 0;

  return (
    <>
      <Table>
        <thead>
          <tr>
            <th>Min</th>
            <th>Mean</th>
            <th>Median</th>
            <th>95th percentile</th>
            <th>97.5th percentile</th>
            <th>99th percentile</th>
            <th>Max</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{min.toFixed(2)}</td>
            <td>{mean.toFixed(2)}</td>
            <td>{median.toFixed(2)}</td>
            <td>{p95.toFixed(2)}</td>
            <td>{p975.toFixed(2)}</td>
            <td>{p99.toFixed(2)}</td>
            <td>{max.toFixed(2)}</td>
          </tr>
          <tr>
            <td>{mapValueToEmoji(min)}</td>
            <td>{mapValueToEmoji(mean)}</td>
            <td>{mapValueToEmoji(median)}</td>
            <td>{mapValueToEmoji(p95)}</td>
            <td>{mapValueToEmoji(p975)}</td>
            <td>{mapValueToEmoji(p99)}</td>
            <td>{mapValueToEmoji(max)}</td>
          </tr>
        </tbody>
      </Table>
    </>
  );
}

export default StatsTable;

import React from 'react';

import ListGroup from 'react-bootstrap/ListGroup';

import './Emoji.css';

const thresholds = [
  {
    match: v => v > 0.85,
    emoji: '🤣',
    name: 'Rolling on the Floor Laughing',
    meaning: '> 0.85',
  },
  {
    match: v => v > 0.5,
    emoji: '😀',
    name: 'Grinning Face',
    meaning: '> 0.5',
  },
  {
    match: v => v > 0.25,
    emoji: '🙂',
    name: 'Slightly Smiling Face',
    meaning: '> 0.25',
  },
  {
    match: v => v > 0,
    emoji: '😐',
    name: 'Neutral Face',
    meaning: '> 0',
  },
  {
    match: v => v === 0,
    emoji: '😶',
    name: 'Face Without Mouth',
    meaning: 'No data',
  },
  {
    match: v => v === undefined,
    emoji: '😶',
    name: 'Face Without Mouth',
    meaning: 'No data',
  },
  {
    match: v => Number.isNaN(v),
    emoji: '😶',
    name: 'Face Without Mouth',
  },
  {
    match: v => v === null,
    emoji: '🤔',
    name: 'Thinking Face',
    meaning: 'Loading',
  },
];
export const mapValueToEmoji = value => {
  const threshold = thresholds.find(t => t.match(value));
  if (!threshold) {
    console.error({value});
  }
  return threshold.emoji;
};
function Emoji(props) {
  const {value} = props;
  const emoji = mapValueToEmoji(value);
  const round = (+value).toFixed(2);
  const hasMeaning = t => t.meaning;
  return (
    <>
      <h1 className={`emoji ${emoji}`}>
        <span role="img" aria-label={`Joy score is ${round}`} title={round}>
          {emoji}
        </span>
      </h1>
      <ListGroup horizontal>
        {thresholds.filter(hasMeaning).map((threshold, idx) => {
          return (
            <ListGroup.Item key={idx}>
              <span role="img" aria-label={threshold.meaning}>
                {threshold.emoji}
              </span>
              &nbsp;
              {threshold.meaning}
            </ListGroup.Item>
          );
        })}
      </ListGroup>
    </>
  );
}
export default Emoji;

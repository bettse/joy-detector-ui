import React, { useState } from 'react';

import { Resizable, Charts, ChartContainer, ChartRow, YAxis, LineChart, styler, Baseline } from "react-timeseries-charts";

import { mapValueToEmoji } from './Emoji'

// https://www.color-hex.com/color-palette/95521
const style = styler([
  {key: 'value', color: '#7a9cd1', width: 2},
]);

const trackerStyle = {
  box: {
    box: { display: 'none', },
    label: {
      fontSize: '48px',
      textAnchor: 'start',
      textAlign: 'center',
      pointerEvents: 'none',
    },
  },
  dot: { display: 'none' },
}

function JoyChart(props) {
  const { series } = props;
  const [selection, setSelection] = useState(undefined);
  const [highlight, setHighlight] = useState(undefined);
  const [tracker, setTracker] = useState(undefined);

  let trackerInfoValues;
  if (tracker) {
    const index = series.bisect(tracker);
    const trackerEvent = series.at(index);
    const value = trackerEvent.get('value')
    if (value) {
      trackerInfoValues = mapValueToEmoji(value)
    }
  }

  const p95 = series.percentile(95, 'value', 'midpoint') || 0;
  const p975 = series.percentile(97.5, 'value', 'midpoint') || 0;
  const p99 = series.percentile(99, 'value', 'midpoint') || 0;

  const baselines = [<Baseline key="p99" axis="y" value={p99} label={`99th percentile (${p99.toFixed(2)})`} position="right"/>];
  if (p99 - p975 > 0.1) {
    baselines.push(<Baseline key="p975" axis="y" value={p975} label={`97.5th percentile (${p975.toFixed(2)})`} position="right"/>);
  }
  if (p975 - p95 > 0.1) {
    baselines.push(<Baseline key="p95" axis="y" value={p95} label={`95th percentile (${p95.toFixed(2)})`} position="right"/>);
  }

  return (
    <Resizable>
      <ChartContainer
        timeRange={series.range()}
        trackerPosition={tracker}
        onTrackerChanged={t => setTracker(t)}
        trackerStyle={trackerStyle}
      >
        <ChartRow
          height='200'
          trackerInfoValues={trackerInfoValues}
        >
          <YAxis id="y" label="Score" min={0.0} max={1.0} width="60" type="linear" format=".2f"/>
          <Charts>
            <LineChart
              axis="y"
              columns={['value']}
              series={series}
              style={style}
              highlight={highlight}
              onHighlightChange={h => setHighlight(h)}
              selection={selection}
              onSelectionChange={s => setSelection(s)}
            />
            {baselines}
          </Charts>
        </ChartRow>
      </ChartContainer>
    </Resizable>
  );
}

export default JoyChart;

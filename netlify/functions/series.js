const moment = require('moment');
const {InfluxDB, typeSerializers} = require('@influxdata/influxdb-client');

const { INFLUXDB_V2_URL, INFLUXDB_V2_ORG, INFLUXDB_V2_TOKEN, INFLUXDB_V2_BUCKET } = process.env;

typeSerializers['dateTime:RFC3339'] = (dt) => (new Date(dt)).getTime()

const queryApi = new InfluxDB({url: INFLUXDB_V2_URL, token: INFLUXDB_V2_TOKEN}).getQueryApi(INFLUXDB_V2_ORG);

exports.handler = async (event, context) => {
  const query = `
  from(bucket: "${INFLUXDB_V2_BUCKET}")
  |> range(start: -10m)
  |> filter(fn: (r) => r["_measurement"] == "joy")
  |> filter(fn: (r) => r["_field"] == "value")
  |> group(columns: ["host"])
  |> yield(name: "_value")
  `;

  try {
    const results = await queryApi.collectRows(query)

    const grouped = results.reduce((acc, record) => {
      const { _time, _value } = record;
      const host = record.host.replace('aiyvision-', '');
      acc[host] = acc[host] || {name: 'joy', tags: {host}, columns: ['time', 'value'], points: []}
      const point = [_time, _value]
      acc[host].points.push(point)
      return acc;
    }, {});

    const series = Object.values(grouped)

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(series)
    }
  } catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: err.toString()
    }
  }
};

